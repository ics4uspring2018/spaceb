import java.util.Scanner;
import java.util.Random;
/**
 * Introduce basic variable and constant declarations
 * 
 * @author Chloe Chin 
 * @version February 2018
 */

//Q: Instead of an independent numberOfPlanets variable, where do you think the number of planets should come from?
// ANS: PlanetarySystem class
//Q: We have an array, but how do we add planets to array?
// ANS: initialize each index of the array

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
public class SpaceAdventure
{
    Planet[] planets = new Planet[13];
    PlanetarySystem planetarySystem;
    
    /** Create a Scanner connection to a file */
    public Scanner load(String filename){
            InputStream file = getClass().getResourceAsStream(filename);
            if(file != null) return new Scanner(file);
            return null;
        }

        /** Create the scanner and read lines of text from it */
    public static void main(String[] args) {
        SpaceAdventure sa = new SpaceAdventure();
            Scanner scan = sa.load("PlanetDescriptionM.txt");
            String line = scan.nextLine();
             SpaceAdventure adventure = new SpaceAdventure();
        adventure.start();
        }
    
    public SpaceAdventure()
    {
        planetarySystem = new PlanetarySystem("Solar System", planets); 
        Planet[] planets = new Planet[13];
        Planet mercury = new Planet("Mercury", " A very hot planet, closest to the sun.");
        planetarySystem.planets[0] = mercury;
        Planet venus = new Planet("Venus", " A very, very hot planet with a toxic atmosphere.");
        planetarySystem.planets[1] = venus;
        Planet earth = new Planet("Earth", " Only known world to habour life.");
        planetarySystem.planets[2] = earth;
        Planet mars = new Planet("Mars", " Cold, dusty place that shares similarities to Earth.");
        planetarySystem.planets[3] = mars;
        Planet jupiter = new Planet("Jupiter", " Most massive planet in our solar system, mostly gaseous.");
        planetarySystem.planets[4] = jupiter;
        Planet saturn = new Planet("Saturn", " Known most for its rings made of ice and rock.");
        planetarySystem.planets[5] = saturn;
        Planet uranus = new Planet("Uranus", " Only giant planet to orbit on its side, nearly at a right angle to its orbital.");
        planetarySystem.planets[6] = uranus;
        Planet neptune = new Planet("Neptune", " A very cold planet, furthest from the sun.");
        planetarySystem.planets[7] = neptune;
        Planet cottenCandy = new Planet("Cotten Candy", " Filled with the best cotten candy in the universe.");
        planetarySystem.planets[8] = cottenCandy;
        Planet discoLand = new Planet("Disco Land", " Plays 80s disco 24/7.");
        planetarySystem.planets[9] = discoLand;
        Planet doggo = new Planet("Doggo", " World run by dogs.");
        planetarySystem.planets[10] = doggo;
        Planet catto = new Planet("Catto", " World run by cats.");
        planetarySystem.planets[11] = catto;
        Planet yellolando = new Planet("Yellolando", " Everything is yellow.");
        planetarySystem.planets[12] = yellolando;
       
       
        
    }
    
    
    //public static void main()
    //{
    //    SpaceAdventure adventure = new SpaceAdventure();
     //   adventure.start();
   // }
    
    public  void start()
    {
        Scanner input = new Scanner(System.in);
        SpaceAdventure adventure = new SpaceAdventure();
        
        displayIntro();
        greetAdventurer();        
        if(!(planetarySystem.planets==null||planetarySystem.planets.length==0))
        {
            System.out.println("Let's go on an adventure!");
            determineDestination();
        }
        
    }
    
    
    private   void displayIntro()
    {
        
        final int numberOfPlanets=13;
        final double circumferenceOfEarth=24859.82; //in miles
        System.out.println("Welcome to the " + planetarySystem.name + "!");
        System.out.println("There are " + planetarySystem.planets.length + " planets to explore.");
        System.out.println("You are currently on Earth, which has a circumference of " + circumferenceOfEarth + " miles.");
    }
    
    
    private  void determineDestination()
    {
        Scanner input = new Scanner(System.in);
        boolean madeDecision = false;
        String decision ="";
        while(madeDecision==false)
        {
            decision = responseToPrompt("Shall I randomly choose a planet for you to visit? (Y or N)");
            
            if(decision.equals("Y"))
            {
                Planet planet = planetarySystem.randomPlanet();
                madeDecision = true;
                if(planet!=null)
                {
                    visit(planet.name);
                }
                else{
                    System.out.println("Sorry, but there are no planets in this system.");
                }
            }
            else if(decision.equals("N"))
            {
                String planetName = responseToPrompt("Ok, name the planet you would like to visit...(Your regular 8 planest or cottenCandy, discoLand, doggo, catto or yellolando");
                for(Planet planet: planetarySystem.planets)
                {
                    if(planetName.equals(planet.name))
                    {
                        madeDecision=true;
                        visit(planetName);
                        break;
                    }
                    
                }
               if(madeDecision==false)
               {
                   System.out.println("Sorry, could you repeat that? I'm case-sensitive.");
                }
            
            }
            else
            {
                System.out.println("Huh? Sorry, I didn't get that.");
            }
        }
        
        
    }
    
    private void visit(String planetName)
    {
        
        for (Planet planet: planetarySystem.planets)
        {
            
            if(planetName.equals(planet.name))
            {
                System.out.println("Travelling to " + planetName + "...");
                System.out.println("Arrived at " + planet.name + "." + planet.description);
            }
        }
    }
    
    private  void greetAdventurer()
    {
        String name = responseToPrompt("What is your name? ");
        System.out.println("Nice to meet you, " + name + ". My name is Eliza.");
        
    }
    
    private static String responseToPrompt(String prompt)
    {
        System.out.println(prompt);
        Scanner input = new Scanner(System.in);
        return input.nextLine();
    }
    
    private static String answer (String decision)
    {
        System.out.println(decision);
        Scanner input = new Scanner(System.in);
        return input.nextLine();
    }
    
    
}
